package ddp2.map;

import ddp2.robot.AbstractRobot;
import ddp2.robot.util.Point;
import java.util.ArrayList;
import java.util.List;

/**
 * mainin aturannya disini
 * > ganti arah dulu baru satu step ke depan arah itu, ya bukan? wkwk
 * > butuh inherit point gak? kalo butuh refactor point ke package baru
 */
public class BidangKotak {
    private List<List> map;
    private int sizeX;
    private int sizeY;
    private final AbstractRobot NO_ROBOT_HERE = null;

    public BidangKotak(int sizeX, int sizeY) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.map = new ArrayList<List>(sizeY);
    }

    public List<List> getMap() {
        return map;
    }

    public void createMap(){
        for(int i = 0; i < sizeY; ++i){
            // always create new object of arraylist with max X
            map.add(new ArrayList<AbstractRobot>(sizeX));
            for(int j = 0; j < sizeX; ++j){
                map.get(i).add(NO_ROBOT_HERE);
            }
        }
    }

    public void remove(int coordX, int coordY){
        // change robot at that pos with null

        try{
            List posY = map.get(coordY);
            posY.set(coordX, NO_ROBOT_HERE);
        } catch (IndexOutOfBoundsException e){
            // say error to user
            System.err.println(e.getMessage());
        }
    }

    public void setPosition(AbstractRobot robot){
        // set pos the robot

        // remove robot
        Point oldPoint = robot.getOldPosition();
        int oldX = oldPoint.getCoordXmap();
        int oldY = oldPoint.getCoordYmap();
        this.remove(oldX, oldY);

        // get now pos
        Point nowPoint = robot.getPosition();
        int nowX = nowPoint.getCoordXmap();
        int nowY = nowPoint.getCoordYmap();

        // set pos in map
        try {
            List posY = map.get(nowY);
            posY.set(nowX, robot);

        } catch (IndexOutOfBoundsException e){
            // say error to user
            System.err.println(e.getMessage());
        }

    }

    @Override
    public String toString(){
        // initiate
        StringBuilder strBuilder = new StringBuilder();

        for(List i:this.map){
            for(Object j:i){
                if (j == null)
                    strBuilder.append("|  |");
                else
                    strBuilder.append(j);
            }
            // newline
            strBuilder.append("\n");
        }
        return strBuilder.toString();
    }

    public int getSizeX() {
        return sizeX;
    }

    public int getSizeY() {
        return sizeY;
    }
}
