package ddp2;

import ddp2.map.BidangKotak;
import ddp2.robot.RobotGedek;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here

        BidangKotak map = new BidangKotak(6, 6);
        RobotGedek robot = new RobotGedek("Gedek", map, 0, 0);
        Scanner in = new Scanner(System.in);
        String command = "";

        map.createMap();

        // put robot
        robot.putRobot();
        printMap(map);

        System.out.println();

        do {
            System.out.print("Command List : move, turnLeft, turnRight, exit \nInsert command : \n");
            command = in.nextLine();
            processInput(command, robot, map);
        } while (!command.equals("exit"));


    }

    public static void printMap(BidangKotak map){
        for(List i:map.getMap()){
            for(Object j:i){
                if (j == null)
                    System.out.print("|  |");
                else
                    System.out.print(j);
            }
            System.out.println();
        }
    }

    private static void processInput(String command, RobotGedek robot, BidangKotak map) {

        switch (command.toUpperCase()) {
            case "MOVE":
                robot.move();
                printMap(map);
                System.out.printf("%s heading to %s\n",robot.getKartesianPosition(), robot.getDirection());
                System.out.println();
                break;
            case "TURNLEFT":
                robot.turnLeft();
                printMap(map);
                System.out.printf("%s heading to %s\n",robot.getKartesianPosition(), robot.getDirection());
                System.out.println();
                break;
            case "TURNRIGHT":
                robot.turnRight();
                printMap(map);
                System.out.printf("%s heading to %s\n",robot.getKartesianPosition(), robot.getDirection());
                System.out.println();
                break;
            case "EXIT":
                System.out.println("Thank you for using this program!");
                break;
            default:
                System.err.println("Command not found!");

        }
    }
}
