package ddp2.robot.util;

/**
 * Moment untuk step step yang dijalankan oleh robot
 * kalau sempet mungkin bisa diaplikasikan backtracking robot
 */
public class Moment {

    private Point historyPoint;
    private char historyDirection;

    public Moment(Point historyPoint, char historyDirection) {
        this.historyPoint = historyPoint;
        this.historyDirection = historyDirection;

    }

    // getter saja, karena memory gak bisa diubah (??)
    public Point getHistoryPoint() {
        return historyPoint;
    }

    public char getHistoryDirection() {
        return historyDirection;
    }
}