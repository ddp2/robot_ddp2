# README #

ROBOT DDP2


### What is this repository for? ###

* Tugas dadakan Pak Alfan
* version: 0.1.1
* Deadline: Sabtu minggu ini

### How do I get set up? ###

* git clone repo ke local (laptop)

	> terminal  : git clone --recursive <link clone> ~/<folder tujuan>

	> windows   : git clone --recursive <link clone> D:\<folder>

	> gitkraken : cari git clone di menu > login pake bitbucket account > ntar ada repo yang bisa di clone

	> folder tujuan terserah

* Java jdk 8
	
* Dependencies

        javax.swing (GUI) belum digunakan.

* How to open

        intellij : sudah ter-setting
        eclipse  : mungkin harus setting pertama kali open project
	
* Deployment instructions

	belum ada

### Guidelines ###
* link belajar cepat: 

        http://rogerdudler.github.io/git-guide/

* kalo butuh git yang GUI, bisa pake gitkraken

        https://www.gitkraken.com/

* tiap commit jangan lupa tulis changelog di "commit message"
* misal butuh branch, buat branch supaya gak nyampur di master. Nanti di pull request supaya bisa di merge ke master.
* belajar git (version control), pakai git karena kerjainnya bisa bareng2